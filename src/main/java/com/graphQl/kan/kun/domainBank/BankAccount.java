package com.graphQl.kan.kun.domainBank;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BankAccount {
	private UUID id;
	private String name;
	private Currency currency;
}
