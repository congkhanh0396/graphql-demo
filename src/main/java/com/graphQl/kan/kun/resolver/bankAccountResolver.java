package com.graphQl.kan.kun.resolver;

import java.util.UUID;

import org.springframework.stereotype.Component;

import com.graphQl.kan.kun.domainBank.BankAccount;
import com.graphQl.kan.kun.domainBank.Currency;

import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class bankAccountResolver implements GraphQLQueryResolver {

	public BankAccount bankAccount(UUID id) {
		log.info("hello", id);
		return BankAccount.builder().id(id).currency(Currency.USD).name("kan").build();
	}
}
